﻿Imports System.ComponentModel
Imports RabbitMQ.Client.MessagePatterns
Imports RabbitMQ.Client
Imports System.Text

Public Delegate Sub UpdateListBox(ByVal message As String)

Public Class frmRabbitMQConsumer

    'WORK QUEUE
    Const QUEUE_NAME As String = "turning-work-queue"

    'PUB/SUB
    Const EXCHANGE_NAME As String = "turning-pubsub-exchange"
    Private temp_queue_name As String = ""

    'ROUTING
    Const ROUTING_EXCHANGE_NAME As String = "turning-routing-exchange"
    Const ROUTING_KEY As String = "turning-test-routing"

    Private isListening As Boolean
    Private WithEvents messageListener As New BackgroundWorker
    Private channel As IModel
    Private connectionFactory As New ConnectionFactory
    Private connection As IConnection
    Private message As String

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub frmRabbitMQConsumer_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        btnSubscribe.Enabled = True
        btnStop.Enabled = False
    End Sub

    Private Sub frmRabbitMQConsumer_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        StopSubscribing()
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        lstConsumedMessages.Items.Clear()
    End Sub

    Private Sub btnSubscribe_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubscribe.Click

        connectionFactory.UserName = "turning"
        connectionFactory.Password = "ttech"
        connectionFactory.VirtualHost = "/"
        connectionFactory.HostName = "localhost" '"172.22.7.153"
        connectionFactory.Protocol = Protocols.FromEnvironment() 'RabbitMQ.Client.Protocols.AMQP_0_9_1
        connectionFactory.Port = AmqpTcpEndpoint.UseDefaultPort
        connection = connectionFactory.CreateConnection
        channel = connection.CreateModel()

        ''WORK QUEUE
        'channel.QueueDeclare(QUEUE_NAME, False, False, False, Nothing)

        ''PUB/SUB
        'channel.BasicQos(0, 1, False)
        'temp_queue_name = channel.QueueDeclare()
        'channel.QueueBind(temp_queue_name, EXCHANGE_NAME, String.Empty)

        'ROUTING
        channel.BasicQos(0, 1, False)
        temp_queue_name = channel.QueueDeclare()
        channel.QueueBind(temp_queue_name, EXCHANGE_NAME, String.Empty)
        channel.QueueBind(temp_queue_name, ROUTING_EXCHANGE_NAME, ROUTING_KEY)

        btnStop.Enabled = True
        btnSubscribe.Enabled = False
        isListening = True

        messageListener.WorkerReportsProgress = True
        messageListener.RunWorkerAsync()

    End Sub

    Private Sub btnStop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStop.Click
        StopSubscribing()
        btnStop.Enabled = False
        btnSubscribe.Enabled = True
    End Sub

    Private Sub StopSubscribing()
        isListening = False
        Try
            channel.Close()
            connection.Close()
        Catch ex As Exception
        End Try
    End Sub

    Private Sub Update(ByVal message As String)
        Me.lstConsumedMessages.Items.Add(message)
    End Sub


#Region "Background Worker"
    Private Sub messageListener_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles messageListener.DoWork
        Dim deliverEventArgs As RabbitMQ.Client.Events.BasicDeliverEventArgs
        Dim props As IBasicProperties
        Dim body As Byte()
        Dim oUpdateListBox As New UpdateListBox(AddressOf Update)

        ''WORK QUEUE
        'Dim consumer As QueueingBasicConsumer = New QueueingBasicConsumer(channel)
        'Dim consumerTag As String = channel.BasicConsume(QUEUE_NAME, False, consumer)

        'PUB/SUB
        Dim subscrip As New Subscription(channel, temp_queue_name)

        While isListening
            Try

                ''WORK QUEUE
                'deliverEventArgs = consumer.Queue.Dequeue()
                'props = deliverEventArgs.BasicProperties
                'body = deliverEventArgs.Body
                'message = System.Text.Encoding.UTF8.GetString(body)
                'Me.Invoke(oUpdateListBox, message)
                'channel.BasicAck(deliverEventArgs.DeliveryTag, False)

                'PUB/SUB
                deliverEventArgs = subscrip.Next()
                body = deliverEventArgs.Body
                message = System.Text.Encoding.UTF8.GetString(body)
                Me.Invoke(oUpdateListBox, message)
                subscrip.Ack(deliverEventArgs)

            Catch ex As Exception
                StopSubscribing()
            End Try
        End While
    End Sub

    Private Sub messageListener_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles messageListener.ProgressChanged
        'Me.lstConsumedMessages.Items.Add(Message)
    End Sub

    Private Sub messageListener_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles messageListener.RunWorkerCompleted
    End Sub

    Private Sub messageListener_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles messageListener.Disposed

    End Sub
#End Region

End Class
